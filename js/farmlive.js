/**
 * @file
 * Global utilities.
 *
 */
(function($, Drupal) {

    'use strict';

    Drupal.behaviors.farmlive = {
        attach: function(context, settings) {
            console.log('Working!')

            if (Hls.isSupported()) {
                // let videoUrl = 'https://cmgw-vpc.lechange.com:8890/LCO/8J0B7DBPAZA3012/3/0/20230216T100841/ac0db3baeeb8d67e688d7bb017057620.m3u8?proto=https';
                // let videoJsOptions = {
                //     // autoplay: true, // 自动播放
                //     autoplay: 'muted',
                //     language: 'zh-CN',
                //     preload: 'auto', // 自动加载
                //     errorDisplay: true, // 错误展示
                //     controls: true,
                //     sources: [{
                //         src: videoUrl,
                //         type: "application/x-mpegURL",
                //     }, ],
                //     // auth_key: this.authKey,
                //     controlBar: {
                //         fullscreenToggle: false
                //     }
                // };

                var video = document.getElementById('livevideo');
                let playuri = document.getElementById('playuri');
                let videoUrl = playuri.src;
                video.style.width = '768px';
                var hls = new Hls();
                hls.on(Hls.Events.MEDIA_ATTACHED, function() {
                    console.log('video and hls.js are now bound together !');
                });
                // hls.on(Hls.Events.MANIFEST_PARSED, function(event, data) {
                //     console.log(
                //         'manifest loaded, found ' + data.levels.length + ' quality level'
                //     );
                // });
                hls.loadSource(videoUrl);
                // bind them together
                hls.attachMedia(video);
                // video.play();

            }

        }
    }
})(jQuery, Drupal)

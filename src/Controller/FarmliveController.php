<?php

namespace Drupal\farmlive\Controller;

use Drupal\Core\Controller\ControllerBase;
// use Drupal\farmlive\EventSubscriber\FarmliveSubscriber;
use Symfony\Component\DependencyInjection\ContainerInterface;
// use Drupal\Core\Render\RendererInterface;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
// use Drupal\node\NodeInterface;

/**
 * Returns responses for FarmLive routes.
 */
class FarmliveController extends ControllerBase {

  /**
   * The farmlive service.
   *
   */
  protected $farmlive;

  /**
   * The farmlive.event_subscriber service.
   *
   * @var \Drupal\farmlive\EventSubscriber\FarmliveSubscriber
   */
  protected $eventSubscriber;

  /**
   * The node storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $nodeStorage;

  /**
   * The entity repository service.
   *
   * @var \Drupal\Core\Entity\EntityRepositoryInterface
   */
  protected $entityRepository;

  /**
   * The node representing the book.
   *
   * @var \Drupal\node\NodeInterface
   */
  protected $node;

    /**
   * Constructs a BookController object.
   *
   * @param \Drupal\node\NodeInterface $node
   *   The node to export.
   *
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   The renderer.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\book\BookManagerInterface $book_manager
   *   The book manager.
   * @param \Drupal\Core\Entity\EntityRepositoryInterface $entity_repository
   *   The entity repository service.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, EntityRepositoryInterface $entity_repository) {
    $this->nodeStorage = $entity_type_manager->getStorage('node');
    $this->entityRepository = $entity_repository;
    // $this->farmlive = $farmlive;
    // $this->node = $node;
    // $this->renderer = $renderer;
  }

  // /**
  //  * {@inheritdoc}
  //  */
  // public static function create(ContainerInterface $container) {
  //   return new static (
  //     $container->get('node'),
  //     // $container->get('farmlive.event_subscriber')
  //   );

  // }
  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('entity.repository')
    );
  }


  /**
   * Builds the response.
   */
  public function build() {

    $build['content'] = [
      '#type' => 'item',
      // '#markup' => $this->t('It works!'),
      // '#markup' => $this->t('It works!' + $this->node->getTitle()),
    ];
    $node = \Drupal::routeMatch ()->getParameter ('node');

    $this->node = $node;
    $build['#title'] = $this->node->getTitle();
    $link = $this->node->get('field_live_m3u8')->getValue();
    $linkUri = $link[0]['uri'];
    // print_r($linkUri);
    // $build['content']['#markup'] = '<a href="'.$linkUri .'" target="_blank">播放</a>';
    // $build['content']['#markup'] = '<video id="livevideo" > </video>';
    // <source src="movie.mp4" type="video/mp4">
    $build['content'][] = [
      '#markup' =>
      // '<div id="playuri" href="'.$linkUri .'" target="_blank"></div>'.
      '<video id="livevideo" controls autoPlay muted>
      <source  id="playuri" src="'.$linkUri .'" >
      </video>',
      '#allowed_tags' => ['video','source']
      ];
    $build['#attached']['library'][] = 'farmlive/farmlive';


    return $build;
  }

}
